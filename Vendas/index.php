<?php
    require "php/Venda.php";

    $venda = new Venda();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Todas as vendas</title>
        <style>
            ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
            }

            li {
            float: right;
            }

            li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            }

            li a:hover {
            background-color: #111;
            }        

            table {
                margin-top: 20px;
            }

            table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            }
            th, td {
            padding: 15px;
            text-align: left;
            }
            div #total{
                margin-top: 15px;
            }
        </style>
    </head>

    <body>
        <ul>
            <li><a href="#home">Vendas</a></li>
            <li><a href="#news">Vendedores</a></li>
        </ul>    
        <div class="conteudo-vendas">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Comissão</th>
                    <th>Valor da Venda</th>
                    <th>Data da venda</th>
                </tr>
<?php
                $valor = 0;
                $comissao = 0;
                foreach($venda->vendasTotais() as $dado){
                    $valor = $valor + $dado['valor_venda'];
                    $comissao = $comissao + $dado['comissao'];
                    echo "
                        <tr>
                            <td>".$dado['id']."</td>
                            <td>".$dado['nome']."</td>
                            <td>".$dado['email']."</td>
                            <td>".$dado['comissao']."</td>
                            <td>".$dado['valor_venda']."</td>
                            <td>".$dado['data_venda']."</td>
                        </tr>
                    ";
                }
?>                
            </table>
            <div id="total">    
                <h3>Valor Total = <?php echo $valor;?></h3>
                <h3>Comissão Total = <? echo $comissao; ?></h3>
            </div>    
        </div>
    </body>
</html>