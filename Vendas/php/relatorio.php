<?php

    use Dompdf\Dompdf;
    require_once("dompdf/autoload.inc.php");
    require_once "Venda.php";

    ob_start();

    $venda = new Venda();
    $valor = 0;
    $vendas = $venda->vendasTotais();
?>

<html>
<head>
<style>
    table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    }
    th, td {
    padding: 15px;
    text-align: left;
    }
</style>
</head>

<body>
    <table>
        <tr>
            <th>Id vendedor</th>
            <th>Nome do vendedor</th>
            <th>Email do vendedor</th>
            <th>Comissao</th>
            <th>Valor da Venda</th>
            <th>Data da Venda</th>
        </tr>


<?php
foreach($vendas as $vendas){
    $valor = $vendas['valor_venda'] + $valor;
    echo "
        <tr>
            <td>".$vendas['id']."</td>
            <td>".$vendas['nome']."</td>
            <td>".$vendas['email']."</td>
            <td>".$vendas['comissao']."</td>
            <td>".$vendas['valor_venda']."</td>
            <td>".$vendas['data_venda']."</td>
        </tr>
    ";
}
    //Obter o html
    $html = ob_get_contents();
    ob_end_clean();//Limpar a memória

    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    $dompdf->stream('relatorio.pdf');

?>
    </table>
    <br><p>Valor Total = <?php echo $valor?></p>
</body>
</html>