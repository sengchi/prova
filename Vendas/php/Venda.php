<?php

require_once "functions.php";

Class Venda {
    
    private $pdo;

    public function __construct(){
        $this->pdo = db_conn();
    }

    public function novaVenda($id_vendedor, $valor_venda){

        $comissao = $valor_venda * 0.085;
        $data = date("Y-m-d");
        $sql = "INSERT INTO vendas (id_vendedores, valor_venda, comissao, data_venda) VALUES (:id, :valor, :comissao, :data_venda)";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id_vendedor);
        $sql->bindValue(":valor", $valor_venda);
        $sql->bindValue(":comissao", $comissao);
        $sql->bindValue(":data_venda", $data);
        $sql->execute();

    }

    public function vendasHoje(){
        
        $sql = "SELECT v.id, v.nome, v.email, ve.comissao, ve.valor_venda, ve.data_venda FROM `vendas` as ve INNER JOIN `vendedores` as v on v.id = ve.id_vendedores WHERE ve.data_venda = :hoje";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":hoje", date("Y-m-d"));
        $sql->execute();

        if(!$sql->rowCount()){
            return Array();
        }

        $dado = $sql->fetchAll();
        return $dado;

    }

    public function vendasTotais(){
        
        $sql = "SELECT v.id, v.nome, v.email, ve.comissao, ve.valor_venda, ve.data_venda FROM `vendas` as ve INNER JOIN `vendedores` as v on v.id = ve.id_vendedores";
        $sql = $this->pdo->prepare($sql);
        $sql->execute();

        if(!$sql->rowCount()){
            return Array();
        }

        $dado = $sql->fetchAll();
        return $dado;

    }

}

?>