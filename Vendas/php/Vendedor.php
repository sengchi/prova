<?php

require_once "functions.php";

Class Vendedor{
    
    private $pdo;

    public function __construct(){
        $this->pdo = db_conn();
    }

    public function adicionarNovoVendedor($nome, $email){

        $sql = "INSERT INTO vendedores (nome, email) VALUES (:nome, :email)";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":email",$email);
        $sql->execute();

    }

    public function nomeVendedor($id){
        $sql = "SELECT nome FROM vendedores WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if(!$sql->rowCount()){
            return '';
        }

        $row = $sql->fetch();
        return $row["nome"];
        
    }

    public function emailVendedor($id){

        $sql = "SELECT email FROM vendedores WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if(!$sql->rowCount()){
            return '';
        }

        $row = $sql->fetch();
        return $row["email"];        
    }

    public function alterarNome($id, $nome){
        
        $sql = "UPDATE vendedores SET nome = :nome WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":id", $id);
        $sql->execute();
    }

    public function alterarEmail($id, $email){

        $sql = "UPDATE vendedores SET email = :email WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":email", $email);
        $sql->bindValue(":id", $id);
        $sql->execute();

    }

    public function deletarVendedor($id){

        $sql = "DELETE FROM vendedores WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();
    }

}

?>